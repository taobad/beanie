// process the loginForm
$('#loginForm').submit(function(event) {

    //Clear notifications
    $('#loginNotification').empty();
    $('#regNotification').empty();

    // get the form data
    var formData = $('#loginForm').serialize();

    // process the form
    $.ajax({
            type: 'POST',
            url: 'http://1-dot-cv-application.appspot.com/beanie?action=login',
            data: formData,
            dataType: 'json',
            encode: true
        })
        // using the done promise callback
        .done(function(data) {
            // console.log(data.payload[0].email);
            // console.log(data.payload[0].access);

            // here we will handle errors and validation messages
            if (data.status === "14") {

                $('#loginNotification').append('<div class="alert alert-danger"> <p>The entered password is incorrect.</p></div>');

            } else if (data.status === "13") {

                $('#loginNotification').append('<div class="alert alert-danger"> <p>There is no user registered under the provided email address.</p></div>');

            } else if (data.status === "ok") {

                // ALL GOOD! just show the success message!
                $('#loginNotification').append('<div class="alert alert-success"> <p>Logged in.</p></div>');

                // usually after form submission, you'll want to redirect
                // window.location = '/thank-you'; // redirect a user to another page

            }
        })
        // using the fail promise callback
        .fail(function(data) {

            // show any errors
            $('#loginNotification').append('<div class="alert alert-danger"> <p>We are sorry but your request cannot be processed at this time. Please, try again in a few minutes.</p></div>');
        });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});


// process the registerForm
$('#registerForm').validetta({
    showErrorMessages : false,
    onValid: function(event) {
        //Clear notifications
        $('#loginNotification').empty();
        $('#regNotification').empty();
        event.preventDefault();

        // get the form data
        var formData = $('#registerForm').serialize();

        // process the form
        $.ajax({
                type: 'POST',
                url: 'http://1-dot-cv-application.appspot.com/beanie?action=addmember',
                data: formData,
                dataType: 'json',
                encode: true
            })
            // using the done promise callback
            .done(function(data) {
                // console.log(data);

                // here we will handle errors and validation messages
                if (data.status === "13") {

                    $('#regNotification').append('<div class="alert alert-danger"> <p>There is already an account registered under the provided email address. Please, try loggin in instead.</p></div>');

                } else if (data.status === "14") {

                    $('#regNotification').append('<div class="alert alert-danger"> <p>Unable to register your email address. Please, try again in a few minutes.</p></div>');

                } else if (data.status === "ok") {

                    // ALL GOOD! just show the success message!
                    $('#regNotification').append('<div class="alert alert-success"> <p>Congratulations. Your account has been created successfully.</p></div>');

                    // usually after form submission, you'll want to redirect
                    // window.location = 'cv.html'; // redirect a user to another page

                }
            })
            // using the fail promise callback
            .fail(function(data) {

                // show any errors
                $('#regNotification').append('<div class="alert alert-danger"> <p>We are sorry but your request cannot be processed at this time. Please, try again in a few minutes.</p></div>');
            });
    },
    onError: function(event) {
        $('#regNotification').empty();
        event.preventDefault();
    }
});


// process the CV Form
$('#cvForm').submit(function(event) {

    // get the form data
   var formData = $('#cvForm').serialize();
   var dir = formData.
    // console.log(formData);
	
    // process the form
    $.ajax({
       type: 'POST',
       url: 'http://localhost:8888/beanie?action=addcv',
       data: formData,
	       dataType: 'json',
	       encode: true
	 })
	 // using the done promise callback
	 .done(function(data) {
	   // here we will handle errors and validation messages
	   // if () {
	   //
	   // }
	  })
	  
	  // using the fail promise callback
	 .fail(function(data) {
	  // show any errors
	
	 });
	
	// stop the form from submitting the normal way and refreshing the page
	event.preventDefault();
	
});

$('#cvForm-pdf').click(funciton() {
	var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#content')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);
});