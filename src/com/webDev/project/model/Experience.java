package com.webDev.project.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class Experience {
	/*@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;*/
	//@Persistent
	private String startDate;
	//@Persistent
	private String endDate;
	//@Persistent
	private String employer;
	//@Persistent
	private String position;
	//@Persistent
	private String roleDescription;
	
	public Experience(String startDate, String endDate,
			String employer, String position, String roleDescription) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.employer = employer;
		this.position = position;
		this.roleDescription = roleDescription;
	}
	
	public Experience() {
		super();
	}

	/*public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}*/

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
}
