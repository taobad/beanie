package com.webDev.project.model;


import javax.jdo.annotations.Element;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.FetchGroups;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(detachable="true")
//@FetchGroup(name="memChildren", members={@Persistent(name="contactDetail"),@Persistent(name="userCV")})
@FetchGroups({@FetchGroup(name="memChildren", members={@Persistent(name="userCV")}) })
public class Member {
	
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
    @Persistent
    private String email;
	@Persistent
	private String password;
	@Persistent
	@Element(dependent = "true")
	private CV userCV;
	
	
	
	public Member(String email, String password) {
		super();
		Key userKey = KeyFactory.createKey(Member.class.getSimpleName(), email);
		this.key = userKey;
		this.email = email;
		this.password = password;
	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CV getUserCV() {
		return userCV;
	}

	public void setUserCV(CV userCV) {
		this.userCV = userCV;
	}
	
}