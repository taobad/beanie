package com.webDev.project.model;

public class LoggedIn {
      String loggedIn;

	public LoggedIn() {
		super();
	}

	public String getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(String loggedIn) {
		this.loggedIn = loggedIn;
	}
      
}
