package com.webDev.project.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable="true")
public class Education {/*
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;*/
	//@Persistent
	private String name;
	//@Persistent
	private String startDate;
	//@Persistent
	private String endDate;
	//@Persistent
	private String qualification;
	//@Persistent
	private String grade;
	
	public Education() {
		super();
	}
	
	

	public Education(String name, String startDate, String endDate,
			String qualification, String grade) {
		super();
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.qualification = qualification;
		this.grade = grade;
	}



	/*public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}*/
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	
}
