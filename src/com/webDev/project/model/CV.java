package com.webDev.project.model;


import java.util.ArrayList;

import java.util.List;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.FetchGroups;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
@FetchGroups({@FetchGroup(name="cvChildren", members={@Persistent(name="contactDetail")}) })
//@FetchGroups({@FetchGroup(name="cvChildren", members={@Persistent(name="skills"), @Persistent(name="schools"), @Persistent(name="experiences")}) })
public class CV {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	@Persistent
	@Element(dependent = "true")
	private ContactDetail contactDetail;
	@Persistent
	private String summary;
	/*@Persistent
	private Text skills;*/
	@Persistent
	//@Element(dependent = "true")
    private Text schools;
	@Persistent
	//@Element(dependent = "true")
    private Text experiences;
	
	
	

	public CV() {
		super();
		this.summary = "";
		//this.skills = new Text("");
		this.schools = new Text("");
		this.experiences = new Text("");
		this.contactDetail = new ContactDetail("","","","","","","","","");
	}


	public String getSummary() {
		return summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}

	public ContactDetail getContactDetail() {
		return contactDetail;
	}

	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	/*public List<String> getSkills() {
		List<String> skillLists = new ArrayList<String>();
		String[] skills = this.skills.getValue().split("\\|");
		for(int i = 0; i < skills.length; i++){
			if(skills[i] != null){
				skillLists.add(skills[i]);
			}
		}
		return skillLists;
	}

	public void setSkills(String skill) {
		if(skill == null || skill.trim().equals("")){
			this.skills = new Text("");
		} else {
			this.skills = new Text(this.skills.getValue() + skill + "|");
		}
	}
	
	public void setSkills(Text skills) {
		this.skills = skills;
	}
	
	public void setSkills(List<String> skills) {
		if(skills == null){
			this.skills = new Text("");
		} else {
			for(String skill : skills){
				this.skills = new Text(this.skills.getValue() + skill + "|");
			}
		}
	}*/
	
	public List<Education> getSchools() {
		List<Education> eduLists = new ArrayList<Education>();
		String[] schoolList = this.schools.getValue().split("\\|");
		for(int i = 0; i < schoolList.length; i++){
			if(schoolList[i] != null){
				String[] schoolDet = schoolList[i].split("\\!");
				Education edu = new Education();
				    edu.setName(schoolDet[0]);
				    edu.setStartDate(schoolDet[1]);
				    edu.setEndDate(schoolDet[2]);
				    edu.setQualification(schoolDet[3]);
				    edu.setGrade(schoolDet[4]);
				    eduLists.add(edu);
				
			}
		}
		return eduLists;
	}

	public void setSchools(List<Education> schools) {
		if(schools == null){
			this.schools = new Text("");
		} else {
			this.schools = new Text("");
			for(Education school : schools){
				String newSchool = school.getName() + "!" + school.getStartDate() + "!" + school.getEndDate() + "!"
								+ school.getQualification() + "!" + school.getGrade() + "|";
				this.schools = new Text(this.schools.getValue() + newSchool);
			}
		}
		
	}
	
	public void setSchools(Education school){
		if(school == null){
			this.schools = new Text("");
		} else {
			String newSchool = school.getName() + "!" + school.getStartDate() + "!" + school.getEndDate() + "!"
					+ school.getQualification() + "!" + school.getGrade() + "|";
			this.schools = new Text(this.schools.getValue() + newSchool);
		}
		//this.savings.add(saving);
	}
	
	
	public List<Experience> getExperiences() {
		List<Experience> expLists = new ArrayList<Experience>();
		String[] expList = this.experiences.getValue().split("\\|");
		for(int i = 0; i < expList.length; i++){
			if(expList[i] != null){
				String[] workDet = expList[i].split("\\!");
				Experience exp = new Experience();
				    exp.setEmployer(workDet[0]);
				    exp.setStartDate(workDet[1]);
				    exp.setEndDate(workDet[2]);
				    exp.setPosition(workDet[3]);
				    exp.setRoleDescription(workDet[4]);
				    expLists.add(exp);
				
			}
		}
		return expLists;
	}

	public void setExperiences(List<Experience> experiences) {
		if(experiences == null){
			this.experiences = new Text("");
		} else {
			this.experiences = new Text("");
			for(Experience experience : experiences){
				String newExperience = experience.getEmployer() + "!" + experience.getStartDate() + "!" + experience.getEndDate() + "!"
								+ experience.getPosition() + "!" + experience.getRoleDescription() + "|";
				this.experiences = new Text(this.experiences.getValue() + newExperience);
			}
		}
		
	}
	
	public void setExperiences(Experience experience){
		if(experience == null){
			this.experiences = new Text("");
		} else  {
			String newExperience = experience.getEmployer() + "!" + experience.getStartDate() + "!" + experience.getEndDate() + "!"
					+ experience.getPosition() + "!" + experience.getRoleDescription() + "|";
			this.experiences = new Text(this.experiences.getValue() + newExperience);
		}
	}
	
  /*public List<String> getSkills() {
		return skills;
	}


	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	
	public void setSkills(String skill) {
		List<String> skills = getSkills();
		skills.add(skill);
		this.skills = skills;
	}


	public List<Education> getSchools() {
		return schools;
	}


	public void setSchools(List<Education> schools) {
		this.schools = schools;
	}
	
	public void setSchools(Education school) {
		List<Education> schools = getSchools();
		schools.add(school);
		this.schools = schools;
	}


	public List<Experience> getExperiences() {
		return experiences;
	}


	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}
	
	public void setExperiences(Experience experience) {
		List<Experience> experiences = getExperiences();
		experiences.add(experience);
		this.experiences = experiences;
	}*/
}