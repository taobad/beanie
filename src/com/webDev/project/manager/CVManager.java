package com.webDev.project.manager;


import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.webDev.project.model.CV;
import com.webDev.project.model.ContactDetail;
import com.webDev.project.model.Education;
import com.webDev.project.model.Experience;
import com.webDev.project.model.Member;
import com.webDev.project.utility.PMF;

public class CVManager {
	
	
	@SuppressWarnings("null")
	public String getCV(String email) throws JSONException {
		JSONObject object = null;
		CV cv  = null;
		
		String status = "";
		String status_message = "";	
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		System.out.println("getting summary for: " + email);
		try{
			cv = new MemberManager().getMember(email).getUserCV();
			status = "ok";
			status_message = "ok";
			
			obj.put("fullname", cv.getContactDetail().getFullname());
			obj.put("email", email);
			obj.put("addressLine1", cv.getContactDetail().getAddressLine1());
			obj.put("addressLine2", cv.getContactDetail().getAddressLine2());
			obj.put("city", cv.getContactDetail().getCity());
			obj.put("postcode", cv.getContactDetail().getPostcode());
			obj.put("country", cv.getContactDetail().getCountry());
			obj.put("county", cv.getContactDetail().getCounty());
			obj.put("landLineNumber", cv.getContactDetail().getLandLineNumber());
			obj.put("mobileNumber", cv.getContactDetail().getMobileNumber());
			
			obj.put("profile", cv.getSummary());
			List<Experience> experiences = new ArrayList<Experience>();
			try{
				experiences = cv.getExperiences();
			} catch(Exception e) {
				if (e instanceof ArrayIndexOutOfBoundsException){
					experiences = null;
				}
			}
			int i = 1;
			if(experiences != null){	
				for (Experience experience : experiences){
					obj.put("companyName"+i, experience.getEmployer());
					obj.put("jobTitle"+i, experience.getPosition());
					obj.put("jobDescription"+i, experience.getRoleDescription());
					obj.put("workStartDate"+i, experience.getEmployer());
					obj.put("workEndDate"+i, experience.getEmployer());
					i++;
				}
			}
			
			List<Education> schools = new ArrayList<Education>(); 
			try{
				schools = cv.getSchools();
			} catch(Exception e) {
				if (e instanceof ArrayIndexOutOfBoundsException){
					schools = null;
				}
			}
			i = 1;
			if(schools != null){	
				for (Education school : schools){
					obj.put("institutionName"+i, school.getName());
					obj.put("courseName"+i, school.getQualification());
					obj.put("grade"+i, school.getGrade());
					obj.put("eduStartDate"+i, school.getStartDate());
					obj.put("eduEndDate"+i, school.getEndDate());
					i++;
				}
			}
			jsonarray.put(obj);
		} catch(Exception e){
			status = "18";
			status_message = "unable to fetch member's cv ";
			System.out.println(e);
		}
		object = new JSONObject();
        object.put("status", status);
        object.put("status_message", status_message);
        object.put("cv", jsonarray);
        
        return object.toString();
	}
	
	
	public String addCV(String email, CV cv) throws JSONException {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		
		JSONObject object = null;
		
		String status = "";
		String status_message = "";	
		String is_saved = "";
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		Transaction tx = null;
		try{
			tx = pm.currentTransaction();
	        tx.begin();
	       
	        //pm.getFetchPlan().addGroup("contactDet");
	        Member mem = new MemberManager().getMember(email);
	        mem = pm.detachCopy(mem);
	        
	        mem.setUserCV(cv);
	         
	        pm.makePersistent(mem);
	        tx.commit();
	        status_message = "ok";
			status = "ok";
			is_saved = "CV saved";
		} catch(Exception e){
			System.out.println(e);
			status_message = "error occured while saving cv, try again.";
			status = "17";
			is_saved = "CV not saved";
		} finally{
			if (tx.isActive()) {
	            tx.rollback();
	        }
			pm.close();
		} 
		
		obj.put("is_saved",is_saved);
		jsonarray.put(obj);
		
		object = new JSONObject();
		object.put("status",status);
		object.put("status_message",status_message);
		object.put("payload",jsonarray);
		
		return object.toString();
		
	}
	
	/*public CV getCV(String email) {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		CV cv = null;
		System.out.println("getting cv for: " + email);
		try{
			//Key userKey = KeyFactory.createKey(CV.class.getSimpleName(), email.trim());
		    cv = (CV)pm.getObjectById(CV.class, email);
		} catch(Exception e){
			return cv;
		} finally{
			pm.close();
		}
		return cv;
	}
	
	
	public boolean addCV(String email, String summary,
			Text skills, Text schools, Text experiences ) {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		boolean registered = false;
		try{
			CV cv = new CV();
			pm.makePersistent(cv);
			registered = true;
			pm.close();
		} catch(Exception e){
			System.out.println(e);
			return registered;
		} 
		
		return registered;
	}*/
	
	/*public void setSummary(String email, String summary){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setSummary(summary);
			pm.makePersistent(cv);
			pm.close();
		}
		
	}
	
	public void getSummary(String email, String summary){
		CV cv = getCV(email); 
		if(cv != null){
			cv.getSummary();
		}
		
	}
	
	public void addOneSkill(String email, String skill){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setSkills(skill);
			pm.makePersistent(cv);
			pm.close();
		}
	}
	
	public void addAllSkills(String email, List<String> skills){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setSkills(skills);
			pm.makePersistent(cv);
			pm.close();
		}
	}
	
	public void addOneSchool(String email, Education school){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setSchools(school);
			pm.makePersistent(cv);
			pm.close();
		}
	}
	
	public void addAllSchools(String email, List<Education> schools){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setSchools(schools);
			pm.makePersistent(cv);
			pm.close();
		}
	}
	
	public void addOneExperience(String email, Experience experience){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setExperiences(experience);
			pm.makePersistent(cv);
			pm.close();
		}
	}
	
	public void addAllExperiences(String email, List<Experience> experiences){
		CV cv = getCV(email); 
		if(cv != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			cv.setExperiences(experiences);
			pm.makePersistent(cv);
			pm.close();
		}
	}
*/	
	
}

/*	public boolean addSummary(String email, String summary) {
PersistenceManager pm, pm2;
pm = PMF.get().getPersistenceManager();	
boolean registered = false;
//Transaction tx = (Transaction) pm.currentTransaction();
//try
//{
  //  ((Transaction) tx).begin(); // Start the PM transaction

    Member member = new MemberManager().getMember(email);
    member = pm.detachCopy(member);
    CV userCV = member.getUserCV();
    userCV = pm.detachCopy(userCV);
	pm.close();		
	
	pm2 = PMF.get().getPersistenceManager();
	try{
		//Member member = new MemberManager().getMember(email);
		//member.getUserCV().setSummary(summary);
		//CV userCV = getCV(email);
		//userCV = pm.detachCopy(userCV);
		
		//...PersistenceManager pm = pmf().getPersistenceManager();
		Query q = pm.newQuery(CV.class, "member == memberKey");
		q.declareParameters("String memberKey");
		Key key = KeyFactory.createKey(Member.class.getSimpleName(), email);
		List<CV> userCVs = (List) q.execute(key);
		
		CV userCV = userCVs.get(0);
		userCV.getSummary();
		member.setUserCV(userCV);
		pm2.makePersistent(member);
		registered = true;
	} catch(Exception e){
		System.out.println("try{ in addSummary: "+e);
		return registered;
	} finally{
		pm.close();
	} 

    //tx.commit(); // Commit the PM transaction
//}
//finally
//{
  //  if (tx.isActive())
    //{
      //  tx.rollback(); // Error occurred so rollback the PM transaction
    //}
//}
return registered;
}*/