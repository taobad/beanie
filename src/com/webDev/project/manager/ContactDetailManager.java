package com.webDev.project.manager;


import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.webDev.project.model.CV;
import com.webDev.project.model.Education;
import com.webDev.project.model.Experience;
import com.webDev.project.model.Member;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.webDev.project.model.ContactDetail;
import com.webDev.project.utility.PMF;

public class ContactDetailManager {
	
	public String getContactDetails(String email) throws JSONException {
		JSONObject object = null;
		ContactDetail ct  = null;
		
		String status = "";
		String status_message = "";	
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		System.out.println("getting Contact details for: " + email);
		try{
			ct = new MemberManager().getMember(email).getUserCV().getContactDetail();
			status = "ok";
			status_message = "ok";
			
			/*obj.put("firstname", ct.getFirstname());
			obj.put("lastname", ct.getLastname());
			obj.put("addressline1", ct.getAddressNumber());
			obj.put("addressline2", ct.getStreet());
			obj.put("city", ct.getCity());
			obj.put("postcode", ct.getPostcode());
			obj.put("country", ct.getCountry());
			obj.put("number", ct.getPhoneNUmber());*/
			
			jsonarray.put(obj);
		} catch(Exception e){
			status = "16";
			status_message = "unable to fetch member details";
		}
		object = new JSONObject();
        object.put("status", status);
        object.put("status_message", status_message);
        object.put("payload", jsonarray);
        
        return object.toString();
	}
	
	
	public String addContactDetails(String email,ContactDetail ct) throws JSONException {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		
		JSONObject object = null;
		
		String status = "";
		String status_message = "";	
		String is_saved = "";
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		Transaction tx = null;
		try{
			tx = pm.currentTransaction();
	        tx.begin();
	       
	        pm.getFetchPlan().addGroup("contactDet");
	        Member mem = new MemberManager().getMember(email);
	        mem = pm.detachCopy(mem);
	        
	        mem.getUserCV().setContactDetail(ct);
	       
	        
	        pm.makePersistent(mem);
	        tx.commit();
	        status_message = "ok";
			status = "ok";
			is_saved = "contact details saved";
		} catch(Exception e){
			System.out.println(e);
			status_message = "error occured while saving contacts details, try again.";
			status = "17";
			is_saved = "contact details not saved";
		} finally{
			if (tx.isActive()) {
	            tx.rollback();
	        }
			pm.close();
		} 
		
		obj.put("is_saved",is_saved);
		jsonarray.put(obj);
		
		object = new JSONObject();
		object.put("status",status);
		object.put("status_message",status_message);
		object.put("payload",jsonarray);
		
		return object.toString();
		
	}

}
