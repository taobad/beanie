package com.webDev.project.manager;


import java.util.ArrayList;
import java.util.UUID;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.webDev.project.model.CV;
import com.webDev.project.model.ContactDetail;
import com.webDev.project.model.LoggedIn;
import com.webDev.project.model.Member;
import com.webDev.project.utility.PMF;

public class MemberManager {
	public Member getMember(String email) {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		Member user = null;
		System.out.println("getting data for: " + email);
		try{
			pm.getFetchPlan().setMaxFetchDepth(-1);
			pm.getFetchPlan().addGroup("memChildren");
			pm.getFetchPlan().addGroup("cvChildren");
			//query.getFetchPlan().addFetchGroup("includeChildren");
			Key userKey = KeyFactory.createKey(Member.class.getSimpleName(), email.trim());
		    user = (Member)pm.getObjectById(Member.class, userKey);
		} catch(Exception e){
			return user;
		} finally{
			pm.close();
		}
		return user;
	}
	
	public String addMember(String email,String password) throws JSONException {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		
		Member member = null;
		
		String status = "";
		String statusMessage = "";
		String em = email;
		boolean registered = false;
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		Transaction tx = null;
		
		if ((email == null) || (email.trim().equals(""))){
			statusMessage = "invalid email";
			status = "11";
		}  else if ((password == null) || (password.trim().equals(""))){
			statusMessage = "invalid password";
			status = "12";
		} else{
			try{
				tx = pm.currentTransaction();
				tx.begin();
		       
				if(getMember(email) != null ){
					statusMessage = "Existing member has a similar email.";
					status = "13";
					registered = false;
				} else{
					member = new Member(email,password);
					
					CV cv = new CV();
					cv.setContactDetail(new ContactDetail("","","","","","","","", ""));
					member.setUserCV(new CV());
					pm.makePersistent(member);
					
					statusMessage = "ok";
					status = "ok";
					registered = true;
				}
				tx.commit();
				
			} catch(Exception e){
				System.out.println(e);
				statusMessage = "Unable to register member, try again.";
				status = "14";
				registered = false;
			} finally{
				if (tx.isActive()) {
		            tx.rollback();
		        }
				pm.close();
			} 
			
			obj.put("email",em);
			obj.put("registered",registered);
			jsonarray.put(obj);
		}
		
		JSONObject object = new JSONObject();
        object.put("status", status);
        object.put("status_message", statusMessage);
        object.put("payload", jsonarray);
        
        return object.toString();
	}
	
	public String addMemberr(String email,String password) throws JSONException {
		PersistenceManager  pm = PMF.get().getPersistenceManager();
		 String status = "";
		Member member = null;

		Transaction tx = null;
		try{
				tx = pm.currentTransaction();
				tx.begin();
		       
				if(getMember(email) != null ){
					status = "13";
				} else{
					member = new Member(email,password);
					
					//CV cv = new CV();
					//cv.setContactDetail(new ContactDetail("","","","","","","","",""));
					member.setUserCV(new CV());
					pm.makePersistent(member);
					
					status = "ok";
				}
				tx.commit();
				
			} catch(Exception e){
				System.out.println(e);
				status = "not ok";
			} finally{
				if (tx.isActive()) {
		            tx.rollback();
		        }
				pm.close();
			} 
		
		return status;
	}

	public ArrayList<Object> authenticateMember(String email, String password) throws JSONException{
		
		ArrayList<Object> memberObjectList = new ArrayList<Object>();
		
		String status = "";
		String statusMessage = "";
		String em = email;
		String access = "denied";
		
		JSONObject obj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		
		Member member = null;
		
		if ((email == null) || (email.trim().equals(""))){
			statusMessage = "invalid email";
			status = "11";
		}  else if ((password == null) || (password.trim().equals(""))){
			statusMessage = "invalid password";
			status = "12";
		} else{
			if( (email != null && !(email.equals(""))) && 
					(password != null && !(password.equals(""))) ){
				member = getMember(email);	
			
				if(member == null){
					statusMessage = "No user with this username";
					status = "13";
					access = "denied";
				} else if(!(member.getPassword().equals(password))){
					statusMessage = "username and password do not match";
					status = "14";
					access = "denied";
				} else{
					statusMessage = "ok";
					status = "ok";
					access = "granted";
				}
				
			}
			obj.put("email",em);
			obj.put("access",access);
			jsonarray.put(obj);
		}
		
		JSONObject object = new JSONObject();
        object.put("status", status);
        object.put("status_message", statusMessage);
        object.put("payload", jsonarray);
        
        LoggedIn login = new LoggedIn();
        login.setLoggedIn(status);
        
        memberObjectList.add(object);
        memberObjectList.add(member);
        memberObjectList.add(login);
        return memberObjectList;
	}
	
	public String authenticateMemberr(String email, String password){
		Member member = null;
		String access = "";
		if( (email != null && !(email.equals(""))) && 
				(password != null && !(password.equals(""))) ){
			member = getMember(email);	
		
			if(member == null){
				access = "denied";
			} else if(!(member.getPassword().equals(password))){
				access = "denied";
			} else{
				access = "granted";
			}
			
		}
		return access;
	}
	
	public Member changePassword(String email, String oldPassword, String newPassword) throws JSONException{
		Member member = (Member) authenticateMember(email, oldPassword).get(1);
		
		if(member != null){
			PersistenceManager  pm = PMF.get().getPersistenceManager();
			member.setPassword(newPassword);
			pm.makePersistent(member);
			pm.close();
		}
		
		return member;
	}

}
