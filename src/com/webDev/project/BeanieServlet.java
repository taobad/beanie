package com.webDev.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.webDev.project.manager.CVManager;
import com.webDev.project.manager.ContactDetailManager;
import com.webDev.project.manager.MemberManager;
import com.webDev.project.model.CV;
import com.webDev.project.model.ContactDetail;
import com.webDev.project.model.Education;
import com.webDev.project.model.Experience;
import com.webDev.project.model.LoggedIn;

@SuppressWarnings("serial")
public class BeanieServlet extends HttpServlet {
	
		
	/*public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH");
		resp.setContentType("application/json; charset=UTF-8");
		
		String action = req.getParameter("action");
		
		if(action.equals("addmember")){
			String password = req.getParameter("rp");
			String email = req.getParameter("re");
			
			try {
				resp.getWriter().println(new MemberManager().addMember(email,password));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("login")){
			String email = req.getParameter("le");
			String password = req.getParameter("lp");
			
			try {
				resp.getWriter().println(new MemberManager().authenticateMember(email, password).get(0).toString());
				//resp.setHeader("Cookie", "SESSIONID="+ req.getSession().getId());
				//req.getSession();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("getcontactdet")){
			String email = req.getParameter("em");;
			
			try {
				resp.getWriter().println(new ContactDetailManager().getContactDetails(email));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("getsummary")){
			String email = req.getParameter("em");;
			
			try {
				resp.getWriter().println(new CVManager().getSummary(email));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} 
	}
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH");
		resp.setContentType("application/json; charset=UTF-8");
		
		HttpSession session = null;
		
		String action = req.getParameter("action");
	    
	    if(action.equals("addmember")){
			String password = req.getParameter("rp");
			String email = req.getParameter("re");
			
			try {
				if(new MemberManager().addMemberr(email, password).equals("ok")){
					session = req.getSession();
					//session.setMaxInactiveInterval(120);
					session.setAttribute("email",email);
					RequestDispatcher rd = req.getRequestDispatcher("cv.html");
			        rd.forward(req,  resp);
				} else {
				resp.getWriter().println(new MemberManager().addMember(email,password));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("login")){
			String email = req.getParameter("le");
			String password = req.getParameter("lp");			
			
			try {
				if(new MemberManager().authenticateMemberr(email, password).equals("granted")){
					session = req.getSession();
					//session.setMaxInactiveInterval(120);
					session.setAttribute("email",email);
					RequestDispatcher rd = req.getRequestDispatcher("cv.html");
			        rd.forward(req,  resp);
				} else {
					ArrayList<Object> objects = new MemberManager().authenticateMember(email, password);
					resp.getWriter().println(objects.get(0).toString());
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("getcontactdet")){
			String email = req.getParameter("em");;
			
			try {
				resp.getWriter().println(new ContactDetailManager().getContactDetails(email));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if(action.equals("addcontactdet")){
	    	
			String email = req.getParameter("eAddress");
	    	
			//Basic Info
        	String fullname = req.getParameter("fName");
	    	String addressLine1 = req.getParameter("aLine1");
	    	String addressLine2 = req.getParameter("aLine2");
	    	String city = req.getParameter("city");
	    	String postcode = req.getParameter("postcode");
	    	String country = req.getParameter("country");
	    	String county = req.getParameter("county");
	    	String landLineNumber = req.getParameter("lNumber");
	    	String mobileNumber =  req.getParameter("mNumber");
	    	
	    	ContactDetail basicinfo = new ContactDetail();
	    	basicinfo.setFullname(fullname);
	    	basicinfo.setAddressLine1(addressLine1);
	    	basicinfo.setAddressLine2(addressLine2);
	    	basicinfo.setCity(city);
	    	basicinfo.setPostcode(postcode);
	    	basicinfo.setCountry(country);
	    	basicinfo.setCounty(county);
	    	basicinfo.setLandLineNumber(landLineNumber);
	    	basicinfo.setMobileNumber(mobileNumber);
	    	
			try {
				resp.getWriter().println(new ContactDetailManager().addContactDetails(email, basicinfo));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} /*else if(action.equals("addsummary")){
	    	
	    	String email = req.getParameter("em");
	    	
	    	String summary = req.getParameter("su");
	    	
	    	try {
				resp.getWriter().println(new CVManager().addSummary(email, summary));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/ else if(action.equals("addcv")){
			String email = req.getParameter("eAddress");
	    	String cvSubmitButton = req.getParameter("cvSubmitBtn");
	    	if (cvSubmitButton.equals("saveBtn")){	    			
	    		session = req.getSession(false);
				if (session != null){	
		    		//Basic Info
		        	String fullname = req.getParameter("fName");
			    	String addressLine1 = req.getParameter("aLine1");
			    	String addressLine2 = req.getParameter("aLine2");
			    	String city = req.getParameter("city");
			    	String postcode = req.getParameter("postcode");
			    	String country = req.getParameter("country");
			    	String county = req.getParameter("county");
			    	String landLineNumber = req.getParameter("lNumber");
			    	String mobileNumber =  req.getParameter("mNumber");
			    	
			    	ContactDetail basicinfo = new ContactDetail();
			    	basicinfo.setFullname(fullname);
			    	basicinfo.setAddressLine1(addressLine1);
			    	basicinfo.setAddressLine2(addressLine2);
			    	basicinfo.setCity(city);
			    	basicinfo.setPostcode(postcode);
			    	basicinfo.setCountry(country);
			    	basicinfo.setCounty(county);
			    	basicinfo.setLandLineNumber(landLineNumber);
			    	basicinfo.setMobileNumber(mobileNumber);
			    	
			    	
			    	//profile
			    	String summary = req.getParameter("profile");
			    	
			    	//experience
			    	String startDate1 = req.getParameter("wsDate1");
			    	String endDate1 = req.getParameter("weDate1");
			    	String employer1 = req.getParameter("cmpName1");
			    	String position1 = req.getParameter("jTitle1");
			    	String roleDescription1 = req.getParameter("jDescription1");
			    	
			    	Experience e1 = new Experience(startDate1, endDate1, employer1, position1,roleDescription1);
		
			    	String startDate2 = req.getParameter("wsDate2");
			    	String endDate2 = req.getParameter("weDate2");
			    	String employer2 = req.getParameter("cmpName2");
			    	String position2 = req.getParameter("jTitle2");
			    	String roleDescription2 = req.getParameter("jDescription2");
			    	Experience e2 = new Experience(startDate2, endDate2, employer2, position2,roleDescription2);
		
			    	String startDate3 = req.getParameter("wsDate3");
			    	String endDate3 = req.getParameter("weDate3");
			    	String employer3 = req.getParameter("cmpName3");
			    	String position3 = req.getParameter("jTitle3");
			    	String roleDescription3 = req.getParameter("jDescription3");
			    	Experience e3 = new Experience(startDate3, endDate3, employer3, position3,roleDescription3);
			    	
			    	List<Experience> experiences = new ArrayList<Experience>();
			    	experiences.add(e1);
			    	experiences.add(e2);
			    	experiences.add(e3);
			    	
			    	//education
			    	String name1 = req.getParameter("insName1");
			    	String eduStartDate1 = req.getParameter("esDate1");
			    	String eduEndDate1 = req.getParameter("eeDate1");
			    	String qualification1 = req.getParameter("crsName1");
			    	String grade1 = req.getParameter("grade1");
			    	Education edu1 = new Education(name1,eduStartDate1,eduEndDate1,qualification1, grade1);
			    	
			    	String name2 = req.getParameter("insName2");
			    	String eduStartDate2 = req.getParameter("esDate2");
			    	String eduEndDate2 = req.getParameter("eeDate2");
			    	String qualification2 = req.getParameter("crsName2");
			    	String grade2 = req.getParameter("grade2");
			    	Education edu2 = new Education(name2,eduStartDate2,eduEndDate2,qualification2, grade2);
			    	
		
			    	String name3 = req.getParameter("insName3");
			    	String eduStartDate3 = req.getParameter("esDate3");
			    	String eduEndDate3 = req.getParameter("eeDate3");
			    	String qualification3 = req.getParameter("crsName3");
			    	String grade3 = req.getParameter("grade3");
			    	Education edu3 = new Education(name3,eduStartDate3,eduEndDate3,qualification3, grade3);
			    	
			    	List<Education> schools = new ArrayList<Education>();
			    	schools.add(edu1);
			    	schools.add(edu2);
			    	schools.add(edu3);
			    	
			    	CV cv = new CV();
			    	cv.setSummary(summary);
			    	cv.setContactDetail(basicinfo);
			    	cv.setExperiences(experiences);
			    	cv.setSchools(schools);
			    	
			    	try {
						resp.getWriter().println(new CVManager().addCV(email, cv));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		} else {
	    			RequestDispatcher rd = req.getRequestDispatcher("cv.html");
			        rd.forward(req,  resp);
	    		}
		    	
	    	} else {
	    		/*FopFactory fopFactory = null;
	    		TransformerFactory tFactory = null;
				
	    		try {
	    			fopFactory = FopFactory.newInstance(new File(""));
					tFactory = TransformerFactory.newInstance();
	    			resp.setContentType("application/pdf");
	    		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, resp.getOutputStream());
	    		    Transformer transformer = tFactory.newTransformer();
	    		    Source src = new StreamSource("foo.fo");
	    		    Result res = new SAXResult(fop.getDefaultHandler());
	    		    transformer.transform(src, res);
	    		} catch (Exception ex) {
	    		        throw new ServletException(ex);
	    		}*/
	    	}
		} else if(action.equals("getcv")){
			String email = req.getParameter("em");
			session = req.getSession(false);
			if (session != null){
				try {
					resp.getWriter().println(new CVManager().getCV(email));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				RequestDispatcher rd = req.getRequestDispatcher("index.html");
		        rd.forward(req,  resp);
			}
		}  

	}
}